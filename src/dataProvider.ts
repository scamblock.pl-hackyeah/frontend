// @ts-nocheck
import crudProvider from "@fusionworks/ra-data-nest-crud";

import {
  CREATE,
  DELETE,
  DELETE_MANY,
  GET_LIST,
  GET_MANY,
  GET_MANY_REFERENCE,
  GET_ONE,
  UPDATE,
  UPDATE_MANY,
} from "react-admin";

const url = "https://api.scamblock.pl";

const provider = crudProvider(url);

export const dataProvider = {
  getList: (resource, params) => provider(GET_LIST, resource, params),
  getOne: (resource, params) => provider(GET_ONE, resource, params),
  getMany: (resource, params) => provider(GET_MANY, resource, params),
  getManyReference: (resource, params) =>
    provider(GET_MANY_REFERENCE, resource, params),
  update: (resource, params) => provider(UPDATE, resource, params),
  updateMany: (resource, params) => provider(UPDATE_MANY, resource, params),
  create: (resource, params) => provider(CREATE, resource, params),
  delete: (resource, params) => provider(DELETE, resource, params),
  deleteMany: (resource, params) => provider(DELETE_MANY, resource, params),
};
