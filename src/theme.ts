import { ThemeOptions } from "@material-ui/core/styles";

export const colors = {
  red: "#F65656",
  blue: "#1FB0CF",
  white: "#EEF2F5",
  black: "#374850",
};

export const theme: ThemeOptions = {
  palette: {
    primary: {
      main: colors.blue,
    },
    secondary: {
      main: colors.white,
    },
    text: {
      primary: colors.black,
      secondary: colors.white,
    },
    error: {
      main: colors.red,
    },
  },
  typography: {
    fontFamily: ["Open Sans"].join(","),
    h1: {
      fontSize: "3rem",
    },
    h2: {
      fontSize: "2.5rem",
    },
  },

  overrides: {
    // MuiBox: {
    //   padingLeft: "0",
    // },
    MuiTableCell: {
      root: {
        height: "100px",
      },
    },
    MuiFilledInput: {
      root: {
        backgroundColor: colors.black,
      },
    },
    MuiTableRow: {
      head: {
        "& > th ": {
          backgroundColor: colors.blue,
          color: `${colors.white} !important`,
          fontWeight: "bold",

          "& > span ": {
            color: `${colors.white} !important`,
            "& > svg ": {
              color: `${colors.white} !important`,
            },
          },
        },
      },
    },
  },
};
