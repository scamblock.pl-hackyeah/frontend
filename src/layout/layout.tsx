import classes from "*.module.css";
import * as React from "react";
import { Layout as RALayout } from "react-admin";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "./appBar";
import Sidebar from "./sidebar";

const useStyles = makeStyles({
  layout: {
    marginTop: "0px",
    paddingTop: "46px",
  },
});

export const Layout: React.FC<any> = (props) => {
  const classes = useStyles();
  return (
    <RALayout
      className={classes.layout}
      {...props}
      appBar={AppBar}
      sidebar={Sidebar}
    />
  );
};

export default Layout;
