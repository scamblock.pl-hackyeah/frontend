import * as React from "react";
import { AppBar as RaAppBar } from "react-admin";

import { makeStyles } from "@material-ui/core/styles";

import { Link } from "react-router-dom";

import Logo from "../logo";

const useStyles = makeStyles({
  header: {
    backgroundColor: "white",
    "& button:first-child": {
      display: "none",
    },
  },
  spacer: {
    flex: 1,
  },
});

export const AppBar: React.FC<any> = (props) => {
  const classes = useStyles();
  return (
        <RaAppBar className={classes.header} {...props}>
          <Link to='/'>
          <Logo />
          </Link>
          <span className={classes.spacer} />
        </RaAppBar>
  );
};

export default AppBar;
