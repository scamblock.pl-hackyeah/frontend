import * as React from "react";
import { Sidebar as RaSidebar } from "react-admin";
import { makeStyles } from "@material-ui/core/styles";

const useSidebarStyles = makeStyles({
  drawerPaper: {
    width: 0,
    "& svg": {
      display: "none",
    },
    "& a": {
      display: "none",
    },
  },
});

export const Sidebar: React.FC<any> = (props) => {
  const classes = useSidebarStyles();

  return <RaSidebar classes={classes} {...props} />;
};

export default Sidebar;
