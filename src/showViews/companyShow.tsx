// @ts-nocheck
import * as React from "react";
import {
  Show,
  SimpleShowLayout,
  ReferenceManyField,
  useShowController,
} from "react-admin";
import { Typography, Box, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { colors } from "../theme";
import { AlertItem } from "./alertItem";

export interface ICompanyShow {}

export interface IAlertItem {
  id: number;
  origin: string;
  points: number;
  ioscoItemId: number;
  createdAt: Date;
  updatedAt: Date;
}



const useStyles = makeStyles({
  root: {
    "&&": {
      padding: 0,
    },
  },
  container: {
    padding: '0 100px'
  },
  header: {
    textAlign: "center",
  },
  alerts: {
    display: "block",
    width: "100%",
    "& > div": {
      display: "block",
    },
  },
  box: {
    "& > div": {
      boxShadow: "0 14px 28px rgba(0,0,0,0.25)",
    padding: '25px'
    },
    
  },
  box2: {
    padding: '25px'
    
    
  }
});

const Alerts: React.FC<{ data: any, ids: any[] }> = ({ data, ids }) => {
  const classes = useStyles();
  if (!ids || !data) return null
  if (ids.length ==0) return <Box pl={8} className={classes.box2}><Typography className={classes.header} variant='h3'>There is no suspicious activity</Typography></Box>
  return ids.map((id: any) => <AlertItem alert={data[id]} />)
}

export const CompanyShow: React.FC<ICompanyShow> = (props) => {
  const {
    record,
  } = useShowController(props);
  const classes = useStyles();
console.log(record)
  return (
    <Box className={classes.container}>
      <Show {...props}>
        <SimpleShowLayout className={classes.root}>
          <Box bgcolor={colors.blue} pl={8} className={classes.box}>
            <Typography
              className={classes.header}
              color="textSecondary"
              variant="h2"
              >
              {record?.company}
            </Typography>
          </Box>
          <ReferenceManyField
            label=""
            reference="alerts"
            target="ioscoItemId"
            className={classes.alerts}
            >
            <Alerts />
          </ReferenceManyField>
        </SimpleShowLayout>
      </Show>
    </Box>
  );
};
