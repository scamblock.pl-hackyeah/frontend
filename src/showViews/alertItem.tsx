import * as React from "react";
import { Typography, Box, Divider, Grid } from "@material-ui/core";
import WarningIcon from "@material-ui/icons/Warning";
import { makeStyles } from "@material-ui/core/styles";
import { IAlertItem } from "./companyShow";
import { colors } from "../theme";

export interface IAlertItemProps {
  alert: IAlertItem;
}

const useStyles = makeStyles({
  icon: {
    height: 64,
    width: 64,
    margin: "auto",
  },
  message: {
    margin: "auto",
  },
  points: {
    color: colors.red,
    fontSize: 80,
    margin: "auto",
  },
});

export const AlertItem: React.FC<IAlertItemProps> = ({ alert }) => {
  const classes = useStyles();

  return (
    <Grid container spacing={3}>
      <Grid justify="center" container item xs={2}>
        <WarningIcon className={classes.icon} color="error" />
      </Grid>
      <Grid justify="center" container item xs={8}>
        <Typography className={classes.message}>{alert.origin}</Typography>
      </Grid>
      <Grid justify="center" container item xs={2}>
        <Typography className={classes.points}>{alert.points}</Typography>
      </Grid>
      <Box width="100%">
        <Divider />
      </Box>
    </Grid>
  );
};
