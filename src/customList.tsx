import * as React from "react";
import { List, Datagrid, TextField, Filter, TextInput } from "react-admin";
import { makeStyles } from "@material-ui/core/styles";

import { Box } from "@material-ui/core";

const useStyles = makeStyles({
  container: {
    padding: '0 100px',
  
  },
});

const CompanyFilter: React.FC<any> = (props) => (
  <Filter {...props}>
    <TextInput label="Company" source="company||$contL" alwaysOn />
    <TextInput label="Jurisdiction" source="jurisdiction||$contL" alwaysOn />
    <TextInput label="Subject" source="subject||$contL" alwaysOn />
    <TextInput label="Comments" source="comments||$contL" alwaysOn />
  </Filter>
);

export const CompanyList: React.FC<any> = (props) => {
  const classes = useStyles();

  return (
    <Box className={classes.container}>

  <List
    filters={<CompanyFilter />}
    {...props}
    actions={null}
    sort={{ field: "points", order: "DESC" }}
    bulkActionButtons={false}


    >
    <Datagrid rowClick="show">
      <TextField source="company" />
      <TextField source="jurisdiction" />
      <TextField source="subject" />
      <TextField source="comments" />
      <TextField label="Alert points" source="points" />
    </Datagrid>
  </List>
    </Box>
  )
  }
