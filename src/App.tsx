import * as React from "react";
import { Admin, Resource } from "react-admin";

import { dataProvider } from "./dataProvider";

import { CompanyList } from "./customList";
import { CompanyShow } from "./showViews/companyShow";
import { theme } from "./theme";
import Layout from "./layout/layout";

function App() {
  return (
    <Admin dataProvider={dataProvider} theme={theme} layout={Layout}>
      <Resource name="iosco" list={CompanyList} show={CompanyShow} />
      <Resource name="alerts" />
    </Admin>
  );
}

export default App;
