import * as React from "react";
import logo from "./static/ScamBlock.png";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  logo: {
    height: 36,
    width: "auto",
    margin: 15
  },
});

export const Logo = () => {
  const classes = useStyles();

  return (
    <div>
      <img className={classes.logo} src={logo} alt="fireSpot" />
    </div>
  );
};

export default Logo;
